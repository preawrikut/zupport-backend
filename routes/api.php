<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RatingsController;
use App\Services\Clients\MapApiService;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('ratings',[RatingsController::class, 'create']);
Route::get('ratings/popular',[RatingsController::class, 'getPopular']);
Route::get('maps/suggests',[MapApiService::class, 'getSuggests']);
Route::get('maps/restaurants',[MapApiService::class, 'getRestaurants']);
// Route::middleware('auth:sanctum')->group(function () {});