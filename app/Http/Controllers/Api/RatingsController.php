<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RatingRequest;
use App\Models\Rating;
use Illuminate\Support\Facades\DB;

class RatingsController extends Controller
{
    public function create(RatingRequest $request)
    {
        try {
            // Attempt to save the data into the database
            $rating = $request->all();
            $user = Rating::create($rating);

            // If the save is successful, return a success response
            return response()->json([
                'status' => 'success',
                'message' => 'Rating created successfully',
                'data' => $user
            ], 201);
                
        } catch (\Exception $e) {
            // If any error occurs during the save process, return an error response
            return response()->json([
                'status' => 'fail',
                'message' => 'Failed to create rating',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function getPopular(Request $request)
    {
        try {
            // Query data from the database
            $query = Rating::select('title', DB::raw('COUNT(*) as count'))
            ->groupBy('title')
            ->orderByDesc('count');

            if( $type = $request->type ) {
                $query->where('type', $type);
            }

            if( $number = $request->number ) {
                $query->limit($number);
            }
            
            $result = $query->get();

            // If the save is successful, return a success response
            return response()->json([
                'status' => 'success',
                'data' => $result
            ], 201);
                
        } catch (\Exception $e) {
            // If any error occurs during the save process, return an error response
            return response()->json([
                'status' => 'fail',
                'error' => $e->getMessage()
            ], 500);
        }
    }
}
