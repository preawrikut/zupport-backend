<?php

namespace App\Services\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RatingRequest;
use App\Models\Rating;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class MapApiService extends Controller
{
    public $apiKey;
    public $urlMapApi;
 
    public function __construct()
    {
        $this->apiKey = 'fb1acce5b7d58f51c16263c97db0232e';
        $this->urlMapApi = 'https://search.longdo.com/mapsearch';
    }

    public function getSuggests(Request $request)
    {
        try {
            // Attempt to save the data into the database
            $searchTerm = $request->searchTerm;

            // Check if the result is already cached
            if (Cache::has('search_' . $searchTerm)) {
                $results = Cache::get('search_suggests_' . $searchTerm);
            } else {
                // If not cached, perform the search query
                $client = new Client();
                $response = $client->get( $this->urlMapApi . '/json/suggest?keyword=' . $searchTerm . '&locale=en&key=' . $this->apiKey);
                $statusCode = $response->getStatusCode(); // Get the status code (e.g., 200, 404, etc.)
                $data = $response->getBody()->getContents();
                $results = json_decode($data);
                
                // Cache the result for a specified duration (e.g., 1 hour)
                Cache::put('search_suggests_' . $searchTerm, $results, now()->addHour());
            }
            
            // If the save is successful, return a success response
            return response()->json([
                'status' => 'success',
                'data' => $results->data
            ], 201);
                
        } catch (\Exception $e) {
            // If any error occurs during the save process, return an error response
            return response()->json([
                'status' => 'fail',
                'message' => 'Failed to connect api longdo map',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function getRestaurants(Request $request)
    {
        try {
            // Attempt to save the data into the database
            $searchTerm = $request->searchTerm;

            // Check if the result is already cached
            if (Cache::has('search_restaurants_' . $searchTerm)) {
                $results = Cache::get('search_' . $searchTerm);
            } else {
                // If not cached, perform the search query
                $client = new Client();
                $response = $client->get( $this->urlMapApi . '/json/search?key=' . $this->apiKey . '&keyword=' . $searchTerm . '&limit=20&area=10&tag=restaurant&span=100km&locale=en');
                $statusCode = $response->getStatusCode(); // Get the status code (e.g., 200, 404, etc.)
                $data = $response->getBody()->getContents();
                $results = json_decode($data);
                
                // Cache the result for a specified duration (e.g., 1 hour)
                Cache::put('search_restaurants_' . $searchTerm, $results, now()->addHour());
            }
            
            // If the save is successful, return a success response
            return response()->json([
                'status' => 'success',
                'data' => $results->data
            ], 201);
                
        } catch (\Exception $e) {
            // If any error occurs during the save process, return an error response
            return response()->json([
                'status' => 'fail',
                'message' => 'Failed to connect api longdo map',
                'error' => $e->getMessage()
            ], 500);
        }
    }
}
